import { LoginPage } from './../pages/login/login';
import { FieldsPage } from './../pages/fields/fields';
import { SignupPage } from './../pages/signup/signup';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//firAuth

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

//firebase Conist
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDTWHDdCvkLQaT05okyAOmiAPtIXVOwQ48",
   

 authDomain: "fast-io-la.firebaseapp.com",
    databaseURL: "https://fast-io-la.firebaseio.com",
    projectId: "fast-io-la",
    storageBucket: "fast-io-la.appspot.com",
    messagingSenderId: "393778820129"
  };
//Endfirebase Conist

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SignupPage,
    FieldsPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    SignupPage,
    FieldsPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
