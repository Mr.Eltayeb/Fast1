import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FieldsPage } from './fields';

@NgModule({
  declarations: [
    FieldsPage,
  ],
  imports: [
    IonicPageModule.forChild(FieldsPage),
  ],
})
export class FieldsPageModule {}
